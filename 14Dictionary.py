class Employee:
    def __init__(self):
        print("constructor called")
        self.x={'one':1, 'two':2}

    def addItem(self,key,value):
        self.x[key]=value


    def getItem(self,key):
        print(self.x.get(key))

    def printValue(self):
        print(self.x)

def main():
    employee = Employee()
    sample = {'four':4}
    employee.addItem("three",3)
    employee.getItem("one")
    employee.printValue()

if __name__ == "__main__": main()

