class Example:
    def __init__(self):
        print("constructor called")
        self.initialVal = 10000

    def addAmount(self,value):
        self.initialVal+=value
        print(self.initialVal)

    def subAmount(self,value):
        self.initialVal-=value

    def printValue(self):
        print(self.initialVal)

def main():
    example = Example()
    example.addAmount(1000)
    example.subAmount(500)
    example.printValue()

if __name__ == "__main__": main()

