class Animal:
     def noise(self): print('snore while sleeping')

class Dog(Animal):
     def noiseOfDog(self): print('bark')

def main():
     noiseExample = Dog()
     noiseExample.noiseOfDog()
     noiseExample.noise()
     
if __name__=="__main__": main()
