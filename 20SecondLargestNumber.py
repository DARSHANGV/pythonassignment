def secondLargestNumber(listExample):
     print(max(n for n in listExample if n != max(listExample)))

def main():
     listExample = input("Enter the space seperated list numbers:\n").rstrip()
     listExample = listExample.split(' ')
     secondLargestNumber(listExample)
     #Below code using sort method
     #listExample.sort()
     #print(listExample)
     #listExample.pop()
     #print(listExample)
     #print(listExample[-1])

if __name__=="__main__":main()
     
