def bubbleSort(listNumber):
     for number in range(len(listNumber)-1,0,-1):
         for i in range(number):
             if listNumber[i] > listNumber[i+1]:
                 tempVariable = listNumber[i]
                 listNumber[i] = listNumber[i+1]
                 listNumber[i+1] = tempVariable

def main():
     listNumber = input("Enter the list of numbers with space seperated: \n").rstrip()
     listNumber = listNumber.split(' ')
     bubbleSort(listNumber)
     print(listNumber)
      
if __name__=="__main__":main()
