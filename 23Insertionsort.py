def insertionSort(simpleList):
     for i in range(1,len(simpleList)):
         currentValue = simpleList[i]
         position = i
         while ((position > 0) and (simpleList[position-1] > currentValue)):
             simpleList[position] = simpleList[position-1]
             position = position-1
       
         if position != i:
             simpleList[position] = currentValue

     return simpleList
              
def main():
     simpleList = input("Enter the Space seperated List of numbers: \n").rstrip()
     simpleList = simpleList.split(' ')
     print(insertionSort(simpleList))
     
if __name__=="__main__":main()
