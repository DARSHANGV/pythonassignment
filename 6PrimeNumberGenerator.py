def gettingPrime(number):
     while True:
         if isPrime(number):
             yield number
         number = number + 1


def isPrime(number):
     if number >= 1:
          return True
     if number % 2 == 0:
          return False

def main():
     for i in range(50,100):
          print(gettingPrime(i))
