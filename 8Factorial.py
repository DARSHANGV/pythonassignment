number = int(input("Enter the valid number"))
fact = 1
if number < 0:
     print("Number is Negative")
elif number == 0:
     print("Factorial of 0 is 1")
else:
     for i in range(1,number+1):
         fact = fact * i
     print(fact)
